# The Raspberry Has You (Einstiegskurs)

## Assembler Programmierung

Tipp:

    git clone https://roehl@bitbucket.org/roehl/the-raspberry-has-you-series-c-assembly-programming.git assembler


### Game

* game_01:      Schere-Stein-Papier (Rock-Paper-Scissor)
* game_02:      Ziegenproblem (Monty-Hall Problem)
* game_03       Siebzehn-Steine Spiel 

