/******************************************************************************
 * @program      : seventeen stones                                           *
 * @course series: the raspberry has                                          *
 * @author:        torsten.roehl@fsg-preetz.org                               *
 * @version:       0.1                                                        *
 * @compile:       gcc -g -o program source.s                                 *
 * @format         :%! asfo.py %                                              *
 ******************************************************************************/
	.global main
	.func main
main:
	push 	{r4, lr}
	sub 	sp, sp,#4
/******************************************************************************/
	@first round without loop
	ldr 	r0,=str0
	bl 	printf
	
	mov 	r5,#17 	@ seventeen stones
	ldr 	r0, =str1
	bl 	printf
	mov 	r1,#0
	mov 	r9,r1
	mov 	r0,r5
	bl 	print_stones
	@ player input
	ldr 	r0, =str3
	bl 	printf
	ldr 	r0,=str4
	mov 	r1, sp
	bl 	scanf
	ldr 	r7, [sp] 	@ player input in r7
	@ pc choice
	mov 	r0,r7
	bl 	pc_turn
	mov 	r8,r0
	
	sub 	r5,r7
	sub 	r5,r8
	
game:
	
	@ clear screen
	ldr 	r0,=str0
	bl 	printf
	@ print game title
	ldr 	r0, =str1
	bl 	printf
	@ print stones
	cmp 	r9,#0 	@color
	moveq 	r1,#1 	@color code
	movne 	r1,#0 	@coor code
	moveq 	r9,#1 	@ color handling
	movne 	r9,#0 	@ color handling
	
	mov 	r0,r5 	@ numer of stones
	bl 	print_stones
	@ print player choice
	mov 	r1, r7
	ldr 	r0,=str5
	bl 	printf
	@ print pc choice
	ldr 	r0,=str10
	mov 	r1,r8
	bl 	printf
	
	@ output player choice
	ldr 	r0, =str3
	bl 	printf
	
	@ input next player choice
	ldr 	r0,=str4
	mov 	r1, sp
	bl 	scanf
	ldr 	r7, [sp]
	@ pc choice
	mov 	r0,r7 	@ player
	bl 	pc_turn
	mov 	r8,r0
	@ adjust stones
	sub 	r5,r7
	sub 	r5,r8
	
	cmp 	r5,#1
	bne 	game
	
	@ game over
	
	@ clear screen
	ldr 	r0,=str0
	bl 	printf
	@ print game title
	ldr 	r0, =str1
	bl 	printf
	@ print stones
	mov 	r0,r5
	bl 	print_stones
	@ print result - it is not possible to win!
	ldr 	r0,=str11
	bl 	printf
	
	@ exit programm
exit:
	add 	sp,sp,#4
	mov 	r0, #0
	pop 	{r4, pc}
	
/*** function *****************************************************************/
	
/*
 * r0 player choice
 * @return pc choice
 */
pc_turn:
	push 	{r7,lr}
	mov 	r2,#4
	sub 	r0,r2,r0
	pop 	{r7,lr}
	bx 	lr
	
/*
 * r0 number of stones
 * print the number of stones
 */
	
print_stones:
	push 	{r4,r5,r6,lr}
	mov 	r6,r1 	@color
	mov 	r5,r0 	@number of stones
	ldr 	r0,=str6
	bl 	printf
	
	mov 	r4,#0
loop:
	cmp 	r6,#0
	ldreq 	r0,=str7
	ldrne 	r0,=str8
	bl 	printf
	add 	r4,r4,#1
	cmp 	r4,r5
	bne 	loop
	
	ldr 	r0,=str9
	bl 	printf
	
	pop 	{r4,r5,r6,lr}
	bx 	lr
	
/*****************************************************************************/
	
	.data
str0:
	.asciz "\033[H\033[J";
str1:
	.asciz "************** GAME ***************\n*        Siebzehn Steine          * \n***********************************\n"
str3:
	.asciz "\tChoose [1/2/3]? "
str4:
	.asciz "%d"
str5:
	.asciz "\n You chose to take %d stone(s)."
str6:
	.asciz "\n"
str7:
	.asciz "\x1B[33m *\x1B[0m"
str8:
	.asciz "\x1B[36m *\x1B[0m"
str9:
	.asciz "\n\n"
str10:
	.asciz "\n The PC chose to take %d stone(s).\n\n"
str11:
	.asciz "\n Game result: You loose :-(\n"
	
