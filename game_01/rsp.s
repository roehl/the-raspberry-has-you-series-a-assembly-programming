/******************************************************************************
 * @program      : rock scissor paper (schere-stein-papier)                   *
 * @course series: the raspberry has you                                      *
 * @author:        torsten.roehl@fsg-preetz.org                               *
 * @version: 0.1                                                              *
 * @ r=114 , s=115 and p=112 in ASCII used for the game logic!                *
 * @ compile:   gcc -g -o rsp rsp.s                                           *
 *****************************************************************************/
	
	.global main
	.func main
main:
	push 	{r4, lr}
	sub 	sp, sp,#4
	bl 	init_rand
	ldr 	r0, =str1
	bl 	printf
/*** player input *************************************************************/
	ldr 	r0, =str2
	mov 	r1, sp
	bl 	scanf
	ldr 	r7, [sp] 	@ player input is now in r7
/*** computer input ***********************************************************/
	bl 	random
	mov 	r8,r0 	@ computer input is now in r8
/*** print player and computer inputs *****************************************/
	mov 	r1,r7 	@ player
	mov 	r2,r8 	@ computer
	ldr 	r0, =str3
	bl 	printf
/*** game *********************************************************************/
	sub 	r9,r7,r8
	ldr 	r0,=str6 	@ load with default win
	@ loose?
	cmp 	r9,#1 	@ scissor - rock
	ldreq 	r0,=str5
	cmp 	r9,#2 	@ rock - paper
	ldreq 	r0,=str5
	cmp 	r9,#-3 	@ paper - scissor
	ldreq 	r0,=str5
	cmp 	r9,#0 	@ draw
	ldreq 	r0,=str4
	
	bl 	printf
	@ exit
	add 	sp,sp,#4
	mov 	r0, #0
	pop 	{r4, pc}
/*** function ******************************************************************/
	
/*
 * function init_rand
 * init random generator with time
 */
init_rand:
	push 	{lr}
	mov 	r0, #0
	bl 	time
	bl 	srand
	pop 	{lr}
	bx 	lr
	
/*
 * function random
 * return: (r0) randon numbers between p=112, r=114, s=115 ascii
 */
random:
	push 	{lr}
	bl 	rand 	@ clib rand
	@ r0 = dividend
	mov 	r1, #3 	@ r1 = divisor
	@ r0 mod r1
	bl 	__aeabi_idivmod
	add 	r0,r1,#112 	@ remainder in r1
	cmp 	r0,#112 	@ paper?
	addne 	r0,r0,#1 	@ if not add 1 to generate rock/scissor
	
	pop 	{lr}
	bx 	lr
	
/*****************************************************************************/
	.data
str1:
	.asciz "********** GAME ***************\n*     Rock Scissor Paper      * \n*******************************\n Choose [r/s/p]? "
str2:
	.asciz "%c"
str3:
	.asciz " (Your choice: %c Computer: %c)\n"
str4:
	.asciz "\n Game result:\tdraw\n"
str5:
	.asciz "\n Game result:\tyou loose :-(\n"
str6:
	.asciz "\n Game result:\tyou win :-)\n"
