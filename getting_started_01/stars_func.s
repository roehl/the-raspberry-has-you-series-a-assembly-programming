/*
 *  draw some stars -  example for two loops :-)
 *  torsten.roehl
 */
	.global main
	.func main
	
main:
	
	mov 	r5,#1
loop:
	
	mov 	r0,r5
	bl 	draw
	
	add 	r5,r5,#1
	cmp 	r5,#stop
	blt 	loop
	
exit:
	mov 	r0,#0
	mov 	r7,#1
	svc 	0
	
/*
 * draw: draws some *'s
 * @param: r0 - number of stars to draw
 */
draw:
	push 	{r5,r6,lr}
	mov 	r6,#0
	mov 	r5,r0
draw_loop: 	
	ldr 	r0, =str1
	bl 	printf 	@print stars
	add 	r6,r6,#1
	cmp 	r6,r5
	blt 	draw_loop
	
	ldr 	r0,=str2
	bl 	printf 	@print newline
	
	pop 	{r5,r6,lr}
	bx 	lr
	
	
	.data
str1:
	.asciz "*"
str2:
	.asciz "\n"
	
	.equ stop,25
