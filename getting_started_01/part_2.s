/*
 * file: part_02.s
 * just one exit routine - ;-)
 */
	.global _exit
	
_exit:
//	mov 	r0, #0 	@return value
	mov 	r7, #1 	@syscall number exit
	svc 	0 	@do the syscall
