/******************************************************************************
 * @program      : monty hall problem                                         *
 * @course series: the raspberry has                                          *
 * @author:        torsten.roehl@fsg-preetz.org                               *
 * @version:       0.1                                                        *
 * @compile:       gcc -g -o montyhall montyhall.s                            *
 ******************************************************************************/
	.global main
	.func main
main:
	push 	{r4, lr}
	sub 	sp, sp,#4
	bl 	init_rand
/*** game info ****************************************************************/
	ldr 	r0, =str1
	bl 	printf
	ldr 	r0, =str2
	bl 	printf
	ldr 	r0, =str3
	bl 	printf
/*** computer input ***********************************************************/
	mov 	r0,#3 	@ three different random numbers
	bl 	random
	mov 	r8,r0 	@ computer choice (0,1,2) in r8
/*** player input *************************************************************/
	ldr 	r0,=str4
	mov 	r1, sp
	bl 	scanf
	ldr 	r7, [sp]
	sub 	r7, r7,#1 	@ player input (0,1,2) in r7
	add 	r1, r7,#1 	@ doors are numbered (1,2,3)
	ldr 	r0,=str5
	bl 	printf
/*** game ********************************************************************/
	mov 	r0,r7
	mov 	r1,r8
	bl 	open_door
	@ door is now open
	mov 	r9,r0 	@ open door in r9
	add 	r1,r9,#49 	@ add ascii distance to 1,2,3
	ldr 	r0,=str6
	bl 	printf
	@ stay or switch
	add 	r1,r7,#49 	@ stay
	
	mov 	r2,#52 	@ switch
	sub 	r2,r2,r9
	sub 	r2,r2,r7
	
	ldr 	r0,=str7
	bl 	printf
	@ you chose door
	ldr 	r0,=str4
	mov 	r1, sp
	bl 	scanf
	ldr 	r7, [sp] 	@ player input is now in r7
	mov 	r1,r7
	sub 	r7,r7,#1 	@ map (0-2)
	ldr 	r0,=str5
	bl 	printf
	@ price is in door
	add 	r1,r8,#49 	@ add ascii distance to 1,2,3
	ldr 	r0,=str8
	bl 	printf
	@ game result
	cmp 	r7,r8
	ldreq 	r0,=str10
	ldrne 	r0,=str9
	bl 	printf
/* exit */
exit:
	add 	sp,sp,#4
	mov 	r0, #0
	pop 	{r4, pc}
	
/*** function *****************************************************************/
	
/*
 * function open_door
 * r0: player input
 * r1: pc     input
 * return: open door
 */
open_door:
	push 	{r7,lr}
	@ step: player != computer
	@ door = 3 -(player + computer)
	cmp 	r0,r1
	addne 	r0,r0,r1
	movne 	r1,#3
	subne 	r0,r1,r0
	bne 	end_door
	@ step: player == computer
	@ door = ( computer + 1 + randon ) mod 3
	mov 	r7,r0
	
	mov 	r0,#2 	@ two different random numbers
	bl 	random
	add 	r0,r0,#1
	
	add 	r0,r0,r7
	cmp 	r0,#2
	subgt 	r0,r0,#3
	
end_door:
	pop 	{r7,lr}
	bx 	lr
	
/*
 * function init_rand
 * init random generator with time
 */
init_rand:
	push 	{lr}
	mov 	r0, #0
	bl 	time
	bl 	srand
	pop 	{lr}
	bx 	lr
/*
 * function rand
 * r0: randon numbers between [0,r0]
 * return: random number (r0)
 */
random:
	push 	{r5,lr}
	mov 	r5,r0 	@ divisor
	bl 	rand 	@ clib rand
	@ r0 = dividend
	mov 	r1, r5 	@ r1 = divisor
	@ r0 mod r1
	bl 	__aeabi_idivmod
	mov 	r0,r1 	@ remainder in r1
	
	pop 	{r5,lr}
	bx 	lr
	
/*****************************************************************************/
	.data
str1:
	.asciz "************ GAME ***************\n*      Monty Hall Problem       * \n*********************************\n"
str2:
	.asciz "\t-------------\n\t| 1 | 2 | 3 |\n\t-------------\n"
str3:
	.asciz "\tChoose [1/2/3]? "
str4:
	.asciz "%d"
str5:
	.asciz "\n You chose door %d.\n"
str6:
	.asciz " Door %c is now open and empty!\n"
str7:
	.asciz " Stay or switch: Choose[%c/%c]? "
str8:
	.asciz " Price is in door %c."
str9:
	.asciz "\n\n Game result: You loose :-(\n"
str10:
	.asciz "\n\n Game result: You win :-)\n"
