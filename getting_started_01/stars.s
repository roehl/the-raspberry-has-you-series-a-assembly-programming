/*
 *  draw some stars
 *  example for two loops :-)
 *  torsten.roehl
 */
	.global main
	.func main
	
main:
	mov 	r5,#1
	
loop:
	
	mov 	r6,#0
/* start inner_loop */
inner_loop:
	ldr 	r0, =str1
	bl 	printf
	add 	r6,r6,#1
	cmp 	r6,r5
	blt 	inner_loop
/* end inner_loop */
	
	ldr 	r0, =str2
	bl 	printf
	
	add 	r5,r5,#1
	cmp 	r5,#stop
	blt 	loop
	
	
exit:
	mov 	r0, #0
	mov 	r7,#1
	svc 	0
	
	.data
str1:
	.asciz "*"
str2:
	.asciz "\n"
	
	.equ stop,25
